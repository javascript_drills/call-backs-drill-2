
const functions = require(`../retriveData.cjs`) ;

const object = "data.json";

const outputPath = "../output";

functions.readFile(object, (error, data) => {

    if (error) {
        console.error(error);
    }

    functions.usersRetrived(data, (error, data1) => {

        if (error) {
            console.error(error);
            return;
        }

        functions.companyGroups(data1, (error, data2) => {

            if (error) {
                console.error(error);
                return;
            }

            functions.companyRetrive(data2, (error, data3) => {

                if (error) {
                    console.error(error);
                    return;
                }

                functions.removeUser(data3, (error, data4) => {

                    if (error) {
                        console.error(error);
                        return;
                    }

                    functions.sortedUsers(data4, (error, data5) => {

                        if (error) {
                            console.error(error);
                            return;
                        }


                        functions.companySwap(data5, (error, data6) => {

                            if (error) {
                                console.error(error);
                                return;
                            }

                            functions.addBirthday(data6);

                        });

                    });


                });

            });

        });

    });

});