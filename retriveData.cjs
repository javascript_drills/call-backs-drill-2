const fs = require(`fs`);
const { get } = require("http");

const path = require(`path`);

const data = require(`./1-employees-callbacks.cjs`);


// 1. Retrieve data for ids : [2, 13, 23].
//     2. Group data based on companies.
//         { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
//     3. Get all data for company Powerpuff Brigade
//     4. Remove entry with id 2.
//     5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
//     6. Swap position of companies with id 93 and id 92.
//     7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.


const object = "data.json";



const outputPath = "../output";


function readFile(object, cb) {
    fs.readFile(object, "utf8", (error, data) => {

        if (error) {
            cb(error, null);
            return;
        }else{

            try{
                data = JSON.parse(data);
    
            }catch(error){
    
                cb(error, null);
                return;
            }
          
    
            cb(null, data);
           
        }
        
    });
    return;

}

function retriveUsers(data, cb) {

    let arrayA = data.employees;

    let userData = arrayA.filter(obj => obj.id === 2 || obj.id === 13 || obj.id === 23);

    try{

        userData = JSON.stringify(userData, null, 4);

    }catch(error){

        cb(error, null);
        return;
    }

    fs.writeFile(path.join(outputPath, "retrivedUsers.json"), userData, (error) => {

        if (error) {
            cb(error, null);
            return;
        }else{
            cb(null, data);
            return;
        }

    });

    return;
}


function groupCompanies(data, cb) {
    
    const employees = data.employees;

    let companiesGroup = employees.reduce((accumulator, obj) => {

        let comp = obj["company"];

        if (accumulator[comp] === undefined) {

            let temp = [];
            temp.push(obj);
            accumulator[comp] = temp;

        } else {

            accumulator[comp].push(obj);
        }

        return accumulator;
    }, {});


    try{

        companiesGroup = JSON.stringify(companiesGroup, null, 4);

    }catch(error){

        cb(error, null);
        return;
    }

    fs.writeFile(path.join(outputPath, "groupCompanies.json"), companiesGroup, (error) => {

        if (error) {

            cb(error, null);
            return;
        }else{

            cb(null, data);
            return;
        }

    });
    return;

}

//     3. Get all data for company Powerpuff Brigade
function retriveCompany(data, cb) {

    const employees = data.employees;

    let companies = employees.filter(obj => obj.company === "Powerpuff Brigade");


    try{

        companies = JSON.stringify(companies, null, 4);

    }catch(error){

        cb(error, null);
        return;
    }


    fs.writeFile(path.join(outputPath, "retriveCompany.json"), companies, (error) => {

        if (error) {

            cb(error, null);
            return;
        }else{
            cb(null, data);
            return;
        }

    });
    return;
}


// 4. Remove entry with id 2.

function removeId(data, cb) {

    const employees = data.employees;

    let filterEmployee = employees.filter(obj => obj.id !== 2);


    try{

        filterEmployee = JSON.stringify(filterEmployee, null, 4);

    }catch(error){

        cb(error, null);
        return;
    }


    fs.writeFile(path.join(outputPath, "removeId.json"), filterEmployee, (error) => {

        if (error) {

            cb(error, null);
            return;
        }else{

            cb(null, data);
            return;

        }

    });

    return;
}


// 5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.


function sortUsers(data, cb) {

    const employees = data.employees;

    let sortedEmployees = employees.sort((a, b) => {

        if (a.company < b.company) {

            return -1;
        }
        else if (a.company === b.company) {

            if (a.id < b.id) {
                return -1;
            }
        }


    });

    try{

        sortedEmployees = JSON.stringify(sortedEmployees, null, 4);

    }catch(error){

        cb(error, null);
        return;
    }


    fs.writeFile(path.join(outputPath, "sortedUsers.json"), sortedEmployees, (error) => {

        if (error) {

            cb(error, null);
            return;
        }else{

            cb(null, data);
            return;
        }

    });

return;

}


//6. Swap position of companies with id 93 and id 92.

function swapCompany(data, cb) {

    const employees = data.employees;

    const emp1 = employees.filter(obj => obj.id === 93);

    const emp2 = employees.filter(obj => obj.id === 92);

    let a = emp2[0]["company"];

    let b = emp1[0]["company"];

    let swappedCompanies = employees.map(obj => {

        if (obj.id === 93) {

            obj.company = a;
        }
        else if (obj.id === 92) {

            obj.company = b;
        }

        return obj;
    });


    try{

        swappedCompanies = JSON.stringify(swappedCompanies, null, 4) ;

    }catch(error){

        cb(error, null);
        return;
    }


    fs.writeFile(path.join(outputPath, "swapedUsers.json"), swappedCompanies, (error) => {

        if (error) {

            cb(error, null);
            return;
        }else{

            cb(null, data);
            return ;
        }

    });
    return;

}

// 7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

function addBirthday(data) {

    const employees = data.employees;

    let employeesBirthday = employees.map(obj => {

        if (obj.id % 2 === 0) {

            obj.birthday = new Date().toString().substring(4, 15);
        }

        return obj;
    });

    try{

        employeesBirthday = JSON.stringify(employeesBirthday, null, 4)

    }catch(error){

        cb(error, null);
        return;
    }

    fs.writeFile(path.join(outputPath, "evenBirthday.json"), employeesBirthday, (error) => {

        if (error) {

            cb(error, null);
            return;
        }

        console.log("All operations executed sucessfully!!");
    });
    return;

}

module.exports.addBirthday = addBirthday ;

module.exports.companySwap = swapCompany ;

module.exports.sortedUsers = sortUsers;

module.exports.removeUser = removeId;

module.exports.companyRetrive = retriveCompany;

module.exports.companyGroups = groupCompanies ;

module.exports.usersRetrived = retriveUsers ;

module.exports.readFile = readFile ;
